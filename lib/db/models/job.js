'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Job extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      const JobModel = models.Job;
      const {
        Corporation,
        CorporationUser,
        JobApplicableInstitute,
        JobRequiredSkill,
        JobQuestion,
        Application
      } = models;
      JobModel.belongsTo(Corporation);
      JobModel.belongsTo(CorporationUser);
      JobModel.hasMany(JobApplicableInstitute)
      JobModel.hasMany(JobRequiredSkill);
      JobModel.hasMany(JobQuestion);
      JobModel.hasMany(Application);
    }
  };
  Job.init({
    title: DataTypes.STRING,
    type: DataTypes.ENUM('JOB', 'INTERN', 'GIG'),
    openForAll: DataTypes.BOOLEAN,
    description: DataTypes.TEXT,
    salaryPerMonth: DataTypes.INTEGER,
    compensationType: DataTypes.ENUM('FIXED', 'NEGOTIABLE',),
    salaryRangeMin: DataTypes.INTEGER,
    salaryRangeMax: DataTypes.INTEGER,
    salaryCurrency: DataTypes.ENUM('INR', 'USD'),
    salaryDescription: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Job',
  });
  return Job;
};