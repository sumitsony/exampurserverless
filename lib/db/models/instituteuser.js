'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class InstituteUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const InstituteUserModel = models.InstituteUser;
      const {
        Institute
      } = models;
      InstituteUserModel.belongsTo(Institute);
      // define association here
    }
  };
  InstituteUser.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    emailVerified: DataTypes.BOOLEAN,
    phoneNumberVerified: DataTypes.BOOLEAN,
    verifiedByAdmin: DataTypes.BOOLEAN,
    role: DataTypes.ENUM('ADMIN', 'MEMBER'),
    invitationAccepted: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'InstituteUser',
  });
  return InstituteUser;
};