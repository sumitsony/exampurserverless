'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ApplicationAnswer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  ApplicationAnswer.init({
    applicationId: DataTypes.INTEGER,
    jobQuestionId: DataTypes.INTEGER,
    answer: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'ApplicationAnswer',
  });
  return ApplicationAnswer;
};