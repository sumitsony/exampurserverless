'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MessageThread extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const MessageThreadModel = models.MessageThread;
      const {
        StudentUser,
        Corporation,
        Institute,
        Message
      }  = models;
      MessageThreadModel.hasMany(Message)
      MessageThreadModel.belongsTo(StudentUser);
      MessageThreadModel.belongsTo(Corporation);
      MessageThreadModel.belongsTo(Institute);
      // define association here
    }
  };
  MessageThread.init({
    jobId: DataTypes.INTEGER,
    status: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'MessageThread',
  });
  return MessageThread;
};