'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Review extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      const ReviewModel = models.Review;
      const {
        Corporation
      } = models;
      ReviewModel.belongsTo(Corporation);
    }
  };
  Review.init({
    name: DataTypes.STRING,
    corporationId: DataTypes.INTEGER,
    profilePic: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Review',
  });
  return Review;
};