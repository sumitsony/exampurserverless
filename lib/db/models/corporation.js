'use strict';
const {
  Model
} = require('sequelize');
const messagethread = require('./messagethread');
module.exports = (sequelize, DataTypes) => {
  class Corporation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const CorporationModel = models.Corporation;
      const {
        CorporationUser,
        Job,
        MessageThread,
        Review
      } = models;
      CorporationModel.hasMany(CorporationUser, {
				foreignKey: "corporationId",
			});
      CorporationModel.hasMany(Job);
      CorporationModel.hasMany(MessageThread);
      CorporationModel.hasMany(Review);
    }
  };
  Corporation.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    website: DataTypes.STRING,
    description: DataTypes.STRING,
    facebookLink: DataTypes.STRING,
    linkedinLink: DataTypes.STRING,  
    instagramLink: DataTypes.STRING,
    twitterLink:  DataTypes.STRING,
    description: DataTypes.STRING,
    logo: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Corporation',
  });
  return Corporation;
};