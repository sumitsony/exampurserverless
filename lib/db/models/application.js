'use strict';
const {
  Model, Sequelize
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Application extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const ApplicationModel = models.Application;
      const {
        Job,
        StudentUser,
        ApplicationAnswer
      } = models;
      ApplicationModel.belongsTo(StudentUser);
      ApplicationModel.belongsTo(Job);
      ApplicationModel.hasMany(ApplicationAnswer);
      // define association here
    }
  };
  Application.init({
    jobId: DataTypes.INTEGER,
    studentId: DataTypes.INTEGER,
    status:  DataTypes.ENUM(
      'APPLIED', 
      'SHORTLISTED',
      'OFFERED',
      'OFFER_ACCEPTED',
      'REJECTED'
    )
  }, {
    sequelize,
    modelName: 'Application',
  });
  return Application;
};