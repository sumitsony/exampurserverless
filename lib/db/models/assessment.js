'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Assessment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const AssessmentModel = models.Assessment;
      const {
        Skill
      } = models;
      AssessmentModel.belongsTo(Skill);
      // define association here
    }
  };
  Assessment.init({
    resume: DataTypes.STRING,
    coverLetter: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Assessment',
  });
  return Assessment;
};