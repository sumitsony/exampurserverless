'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StudentEducationHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const StudentEducationHistory = models.StudentEducationHistory;
      const {
        StudentUser
      }  = models;
      StudentEducationHistory.belongsTo(StudentUser, {foreignKey: 'studentId'});
    }
  };
  StudentEducationHistory.init({
    studentId: DataTypes.INTEGER,
    startYear: DataTypes.INTEGER,
    endYear: DataTypes.INTEGER,
    stream: DataTypes.STRING,
    score: DataTypes.REAL,
    scoreScale: DataTypes.STRING // TODO; convert to enum later
  }, {
    sequelize,
    modelName: 'StudentEducationHistory',
  });
  return StudentEducationHistory;
};