'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StudentSkill extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      const StudentSkillModel = models.StudentSkill;
      const {
        StudentUser,
        Skill
      } = models;
      StudentSkillModel.belongsTo(StudentUser);
      StudentSkillModel.belongsTo(Skill);
    }
  };
  StudentSkill.init({
    studentId: DataTypes.INTEGER,
    skillId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'StudentSkill',
  });
  return StudentSkill;
};