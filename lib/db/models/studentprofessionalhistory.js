'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StudentProfessionalHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const StudentProfessionalHistoryModel = models.StudentProfessionalHistory;
      const {
        StudentUser
      }  = models;
      StudentProfessionalHistoryModel.belongsTo(StudentUser, {foreignKey: 'studentId'});
    }
  };
  StudentProfessionalHistory.init({
    type: DataTypes.ENUM('JOB'),
    studentId: DataTypes.INTEGER,
    organization: DataTypes.STRING,
    role: DataTypes.STRING,
    locationId: DataTypes.NUMBER,
    startDate: DataTypes.DATE,
    startDate: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'StudentProfessionalHistory',
  });
  return StudentProfessionalHistory;
};