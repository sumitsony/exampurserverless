'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CorporationUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const CorporationUserModel = models.CorporationUser;
      const {
        Corporation
      } = models;
      CorporationUserModel.belongsTo(Corporation, {
				foreignKey: "corporationId",
			});
    }
  };
  CorporationUser.init({
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    phoneNumber: {
      type: DataTypes.STRING
    },
    corporationId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phoneNumberVerified: DataTypes.BOOLEAN,
    verifiedByAdmin: DataTypes.BOOLEAN,
    invitationAccepted: DataTypes.BOOLEAN,
    role: DataTypes.ENUM('ADMIN', 'MEMBER')
  }, {
    sequelize,
    modelName: 'CorporationUser',
  });
  return CorporationUser;
};