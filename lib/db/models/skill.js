'use strict';
const {
  Model
} = require('sequelize');
const jobrequiredskill = require('./jobrequiredskill');
module.exports = (sequelize, DataTypes) => {
  class Skill extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const SkillModel = models.Skill;
      const {
        StudentUser,
        StudentSkill,
        JobRequiredSkill,
        Assessment
      } = models;
      SkillModel.belongsToMany(StudentUser, {through: StudentSkill});
      SkillModel.hasOne(JobRequiredSkill);
      SkillModel.hasMany(Assessment);
    }
  };
  Skill.init({
    name: DataTypes.STRING,
    status: DataTypes.ENUM('ACTIVE', 'INACTIVE')
  }, {
    sequelize,
    modelName: 'Skill',
  });
  return Skill;
};