'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class JobRequiredSkill extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      const JobRequiredSkillModel = models.JobRequiredSkill;
      const {
        Job,
        Skill
      } = models;
      JobRequiredSkill.belongsTo(Job);
      JobRequiredSkill.belongsTo(Skill);
    }
  };
  JobRequiredSkill.init({
    jobId: DataTypes.INTEGER,
    skillId: DataTypes.INTEGER,
    percentile: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'JobRequiredSkill',
  });
  return JobRequiredSkill;
};