'use strict';
const {
  Model
} = require('sequelize');

const {socialMediaProfiles} = require('../../constants');
module.exports = (sequelize, DataTypes) => {
  class StudentSocialProfile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const StudentSocialProfileModel = models.StudentSocialProfile;
      const {
        StudentUser
      }  = models;
      StudentSocialProfileModel.belongsTo(StudentUser, {foreignKey: 'studentId'});
    }
  };
  StudentSocialProfile.init({
    studentId: DataTypes.INTEGER,
    link: DataTypes.STRING,
    type: DataTypes.ENUM(Object.keys(socialMediaProfiles))
  }, {
    sequelize,
    modelName: 'StudentSocialProfile',
  });
  return StudentSocialProfile;
};