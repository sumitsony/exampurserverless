"use strict";
const { Model } = require("sequelize");
const studentskill = require("./studentskill");
module.exports = (sequelize, DataTypes) => {
	class StudentUser extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
			const StudentUserModel = models.StudentUser;
			const {
				StudentSkill,
				StudentInterests,
				StudentProfessionalHistory,
				StudentEducationHistory,
				StudentSocialProfile,
				Application,
				Institute,
				MessageThread,
				Skill,
			} = models;
			StudentUserModel.hasMany(StudentProfessionalHistory, {
				foreignKey: "studentId",
			});
			StudentUserModel.hasMany(StudentEducationHistory, {
				foreignKey: "studentId",
			});
			StudentUserModel.hasMany(StudentSocialProfile, {
				foreignKey: "studentId",
			});
			StudentUserModel.hasMany(Application);
			StudentUserModel.belongsTo(Institute, {
				foreignKey: { name: "instituteId" },
			});
			StudentUserModel.hasMany(MessageThread);
			StudentUserModel.belongsToMany(Skill, { through: StudentSkill });
		}
	}
	StudentUser.init(
		{
			title: DataTypes.STRING,
			firstName: DataTypes.STRING,
			lastName: DataTypes.STRING,
			email: DataTypes.STRING,
			phoneNumber: DataTypes.STRING,
			emergencyPhoneNumber: DataTypes.STRING,
			currentAddress: DataTypes.TEXT,
			currentCity: DataTypes.STRING,
			pincode: DataTypes.INTEGER,
			currentCollegeName: DataTypes.INTEGER,
			instituteId: DataTypes.INTEGER,
			profilePicture: DataTypes.STRING,
			status: DataTypes.ENUM("ACTIVE", "INACTIVE"),
			instituteVerified: DataTypes.BOOLEAN,
			emailVerified: DataTypes.BOOLEAN,
			phoneNumberVerified: DataTypes.BOOLEAN,
			lookingForJob: DataTypes.BOOLEAN,
			lookingForInternship: DataTypes.BOOLEAN,
			lookingForOffice: DataTypes.BOOLEAN,
			lookingForRemote: DataTypes.BOOLEAN
		},
		{
			sequelize,
			modelName: "StudentUser",
		}
	);
	return StudentUser;
};
