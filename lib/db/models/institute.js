'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Institute extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const InstituteModel = models.Institute;
      const {
        InstituteUser,
        StudentUser
      } = models;
      InstituteModel.hasMany(InstituteUser);
      InstituteModel.hasMany(StudentUser, {foreignKey: 'instituteId'});
      // define association here
    }
  };
  Institute.init({
    name: DataTypes.STRING,
    affiliateUniversityName: DataTypes.STRING,
    placementCellEmail: DataTypes.STRING,
    logo: DataTypes.STRING,
    website: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Institute',
  });
  return Institute;
};