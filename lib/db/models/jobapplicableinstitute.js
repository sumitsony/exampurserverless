'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class JobApplicableInstitute extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const JobApplicableInstituteModel = models.JobApplicableInstitute;
      const {
        Job,
        Institute
      } = models;
      JobApplicableInstitute.belongsTo(Job);
      JobApplicableInstitute.belongsTo(Institute);
      // define association here
    }
  };
  JobApplicableInstitute.init({
    jobId: DataTypes.INTEGER,
    instituteId: DataTypes.INTEGER,
    approvedByInstitute: DataTypes.BOOLEAN,
    status: DataTypes.ENUM('ACTIVE', 'INACTIVE')
  }, {
    sequelize,
    modelName: 'JobApplicableInstitute',
  });
  return JobApplicableInstitute;
};