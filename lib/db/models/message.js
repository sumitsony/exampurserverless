'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Message extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const MessageModel = models.Message;
      const {
        MessageThread
      } = models;
      MessageModel.belongsTo(MessageThread);
      // define association here
    }
  };
  Message.init({
    senderId: DataTypes.INTEGER,
    senderType: DataTypes.ENUM('STUDENT', 'CORPORATE_USER', 'INSTITUTE_USER'),
    status: DataTypes.BOOLEAN,
    receiverId: DataTypes.ENUM('STUDENT', 'CORPORATE_USER', 'INSTITUTE_USER'),
    payload: DataTypes.STRING(100)
  }, {
    sequelize,
    modelName: 'Message',
  });
  return Message;
};