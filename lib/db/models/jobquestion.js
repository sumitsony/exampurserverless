'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class JobQuestion extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const JobQuestionModel = models.JobQuestion;
      const {
        Job
      } = models;
      JobQuestionModel.belongsTo(Job);
      // define association here
    }
  };
  JobQuestion.init({
    question: DataTypes.STRING,
    jobId: DataTypes.NUMBER,
    sequenceNumber: DataTypes.NUMBER
  }, {
    sequelize,
    modelName: 'JobQuestion',
  });
  return JobQuestion;
};