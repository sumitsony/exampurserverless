"use strict";
module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable("StudentEducationHistories", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER,
			},
			studentId: {
				type: Sequelize.INTEGER,
			},
			startYear: Sequelize.INTEGER,
			endYear: Sequelize.INTEGER,
			stream: Sequelize.STRING,
			score: Sequelize.REAL,
			scoreScale: Sequelize.STRING, // TODO; convert to enum later
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE,
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE,
			},
		});
	},
	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable("StudentEducationHistories");
	},
};
