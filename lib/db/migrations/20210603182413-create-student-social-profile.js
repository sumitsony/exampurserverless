'use strict';
const {socialMediaProfiles} = require('../../constants');
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('StudentSocialProfiles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      studentId: {
        type: Sequelize.INTEGER,
        reference: {
          model: 'StudentUsers'
        }
      },
      link: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.ENUM(Object.keys(socialMediaProfiles))
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('StudentSocialProfiles');
  }
};