'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('StudentUsers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      firstName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      phoneNumber: {
        type: Sequelize.STRING
      },
      emergencyPhoneNumber: {
        type: Sequelize.STRING
      },
      currentAddress: {
        type: Sequelize.TEXT
      },
      currentCity: {
        type: Sequelize.STRING
      },
      pincode: {
        type: Sequelize.INTEGER
      },
      currentCollegeName: {
        type: Sequelize.STRING
      },
      instituteId: {
        type: Sequelize.INTEGER
      },
      profilePicture: {
        type: Sequelize.STRING
      },
      instituteVerified: {
        type: Sequelize.BOOLEAN
      },
      emailVerified: {
        type: Sequelize.BOOLEAN
      },
      phoneNumberVerified: {
        type: Sequelize.BOOLEAN
      },
      status: {
        type: Sequelize.ENUM('ACTIVE', 'INACTIVE')
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('StudentUsers');
  }
};