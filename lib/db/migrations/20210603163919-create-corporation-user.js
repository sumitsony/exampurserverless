'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('CorporationUsers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        allowNull: false,
        type: Sequelize.STRING
      },
      lastName: {
        allowNull: false,
        type: Sequelize.STRING
      },
      email: {
        unique: true,
        type: Sequelize.STRING
      },
      corporationId: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      phoneNumber: {
        type: Sequelize.STRING
      },
      verifiedByAdmin: { // TODO: add this to frontend
        type: Sequelize.BOOLEAN
      },
      invitationAccepted: {
        type: Sequelize.BOOLEAN,
      },
      phoneNumberVerified: {
        type: Sequelize.BOOLEAN
      },
      role: {
        type: Sequelize.ENUM('ADMIN', 'MEMBER')
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('CorporationUsers');
  }
};