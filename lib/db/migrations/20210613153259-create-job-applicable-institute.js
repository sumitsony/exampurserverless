'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('JobApplicableInstitutes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      jobId: {
        type: Sequelize.INTEGER
      },
      instituteId: {
        type: Sequelize.INTEGER
      },
      approvedByInstitute: {
        type: Sequelize.BOOLEAN
      },
      status: {
        type: Sequelize.ENUM('ACTIVE', 'INACTIVE')
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('JobApplicableInstitutes');
  }
};