module.exports = {
    GITHUB: 'Github', 
    PERSONAL_BLOG: 'Personal Blog', 
    PLAYSTORE: 'Playstore',
    INSTAGRAM: 'Instagram', 
    FACEBOOK: 'Facebook', 
    TWITTER: 'Twitter', 
    LINKEDIN: 'LinkedIn',
    CODECHEF: 'Codechef', 
    HACKERRANK: 'HackerRank',
    HACKEREARTH: 'Hackerearth', 
    BEHANCE: 'Behance', 
    DRIBBLE: 'Dribble',
};