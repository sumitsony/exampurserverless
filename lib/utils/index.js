const getCommonCORSHeaders = require('./getCommonCORSHeaders');
const generateResponse = require('./generateResponse');
module.exports = {
    getCommonCORSHeaders,
    generateResponse
};