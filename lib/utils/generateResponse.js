const getCommonCORSHeaders = require('./getCommonCORSHeaders');
module.exports = ({statusCode = 200, headers = null, body = null}) => {
    if (statusCode == 500 && headers == null && body == null) {
        return {
            statusCode: 500,
            headers: {
                ...getCommonCORSHeaders()
            },
            body: JSON.stringify({error: 'Internal Error'})
        };
    } else {
        return {
            statusCode,
            headers: {
                ...getCommonCORSHeaders(),
                ...headers        
            },
            body
        };
    }
}