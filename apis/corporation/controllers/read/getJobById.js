// send canApply
const { Job, JobQuestion } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		jobId: event.queryStringParameters.jobId || 0,
	};
}

function getApplicability(jobId, studentId) {
	return true; // implement is applicable
}
module.exports.main = async (event, context) => {
	console.log(event);
	const { jobId, studentId } = getInputs(event);
	const jobDetails = await Job.findOne({
		where: {
			id: jobId,
		},
		include: {
			model: JobQuestion
		},
	});
	jobDetails.studentApplicable = applicability;
	return {
		statusCode: 200,
		body: JSON.stringify(jobDetails),
	};
};
