// send canApply
const {
	Corporation
} = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		corporationId: event.queryStringParameters.corporationId || 0,
    };
}
module.exports.main = async (event, context) => {
	console.log(event);
	const { 
        corporationId
    } = getInputs(event);
	const corporationDetails = await Corporation.findOne({
		where: {
			id: corporationId
		}
	});
	return {
		statusCode: 200,
		body: JSON.stringify(corporationDetails),
	};
};
