const { CorporationUser } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		corporationId: event.queryStringParams.corporationId || "",
		limit: event.queryStringParams.limit || 10,
		offset: event.queryStringParams.offset || 0
	};
}

module.exports.main = async (event, context) => {
	console.log(event);
	const { corporationId, limit, offset } = getInputs(event);
	let corporationUsers = [];
	if (corporationId) {
		corporationUsers = await CorporationUser.findAll({
			where: {
				corporationId
			},
			limit: Math.min(limit, 10),
            offset
		});
	}
	return {
		statusCode: 200,
		body: JSON.stringify(corporationUsers),
	};
};
