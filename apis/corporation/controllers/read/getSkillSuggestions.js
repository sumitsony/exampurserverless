const { Skill, Sequelize } = require("@campushyre/lib/db/models");
const { Op } = Sequelize;

function getInputs(event) {
	return {
		suggestQuery: event.queryStringParams.suggestQuery || "",
	};
}

module.exports.main = async (event, context) => {
	console.log(event);
	const { suggestQuery } = getInputs(event);
	let skillSuggestions = [];
	if (suggestQuery) {
		skillSuggestions = await Skill.findAll({
			attributes: ["id", "name"],
			where: {
				status: "ACTIVE",
				name: {
					[Op.iLike]: `${suggestQuery}%`,
				},
			},
			limit: 10,
		});
	}
	return {
		statusCode: 200,
		body: JSON.stringify(skillSuggestions),
	};
};
