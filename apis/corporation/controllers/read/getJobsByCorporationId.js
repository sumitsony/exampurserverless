const { Job } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		corporationId: event.queryStringParams.corporationId || "",
	};
}

module.exports.main = async (event, context) => {
	console.log(event);
	const { corporationId, limit, offset } = getInputs(event);
	let jobsByCorporationId = [];
	if (corporationId) {
		jobsByCorporationId = await Job.findAll({
			where: {
				corporationId
			},
			limit: Math.min(limit, 10),
            offset
		});
	}
	return {
		statusCode: 200,
		body: JSON.stringify(jobsByCorporationId),
	};
};
