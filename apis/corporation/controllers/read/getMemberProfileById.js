const {
	CorporationUser
} = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		corporationUserId: event.queryStringParameters.corporationUserId || 0,
    };
}
module.exports.main = async (event, context) => {
	console.log(event);
	const { 
        corporationUserId
    } = getInputs(event);
	const corporationUserProfile = await CorporationUser.findOne({
		where: {
			id: corporationUserId
		}
	});
	return {
		statusCode: 200,
		body: JSON.stringify(corporationUserProfile),
	};
};
