const { CorporationUser } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
        corporationId: event.pathParams.corporationId,
        body: event.body
	};
}

module.exports.main = async (event, context) => {
    try{
        console.log(event);
        const { corporationId, body } = getInputs(event);
        // TODO: validate OTP
        const savedCorporationUser = await CorporationUser.create({
            corporationId,
            ...body,
        });
        return {
            statusCode: 200,
            body: JSON.stringify(savedCorporationUser)
        };    
    }
    catch(error) {
        return {
            statusCode: 500,
            body: JSON.stringify({
                error: error.message
            }),
        }
    }

};
