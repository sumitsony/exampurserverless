const { Job } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
        corporationId: event.pathParams.corporationId,
        body: event.body
	};
}

module.exports.main = async (event, context) => {
    try{
        console.log(event);
        const { corporationId, body } = getInputs(event);
        // TODO: validate OTP
        const savedJob = await Job.create({
            corporationId,
            ...body,
        });
        return {
            statusCode: 200,
            body: JSON.stringify(savedJob)
        };    
    }
    catch(error) {
        return {
            statusCode: 500,
            body: JSON.stringify({
                error: error.message
            }),
        }
    }

};
