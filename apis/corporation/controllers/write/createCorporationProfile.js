const { Corporation, CorporationUser, CorporationInvites } = require("@campushyre/lib/db/models");
const { generateResponse } = require("@campushyre/lib/utils");

function getInputs(event) {
	return {
        body: event.body
	};
}

module.exports.main = async (event, context) => {
    try{
        console.log(event);
        const { body } = getInputs(event);
        const {
            corporationUser,
            corporationProfile,
            invites = []
        } = body;
        if (!corporationProfile || !corporationUser) {
            return generateResponse({statusCode: 500})
        }
        let savedCorporationProfile, savedCorporationUser, savedCorporationInvites;
        try {
            savedCorporationProfile = await Corporation.create(corporationProfile);
        } catch(e) {
            console.error(e);
            return generateResponse({statusCode: 500});
        }
        try {
            savedCorporationUser = await CorporationUser.create({
                ...corporationUser,
                corporationId: savedCorporationProfile.id,
                role: 'ADMIN',
                verifiedByAdmin: true,
                invitationAccepted: true
            });
        }
        catch(e) {
            console.error(e);
            return generateResponse({statusCode: 500})
        }
        try {
            savedCorporationInvites = await CorporationUser.bulkCreate(invites.map(invite => {
                return {
                    ...invite,
                    corporationId: savedCorporationProfile.id,
                    role: 'MEMBER',
                    verifiedByAdmin: true,
                    invitationAccepted: false
                };
            }));
            // TODO: implement using SES
            // await sendInviteEmails(invites);
        }
        catch(e) {
            console.error(e);
            return generateResponse({statusCode: 500})
        }
        return {
            statusCode: 200,
            body: JSON.stringify({
                corporation: savedCorporation,
                corporationUser: savedCorporationUser,
                invites: savedCorporationInvites
            })
        };    
    }
    catch(error) {
        return {
            statusCode: 500,
            body: JSON.stringify({
                error: error.message
            }),
        }
    }
};
