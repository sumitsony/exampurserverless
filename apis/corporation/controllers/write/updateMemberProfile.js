const { CorporationUser } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		corporationId: event.pathParams.corporationId,
        userId: event.pathParams.userId,
		body: event.body,
	};
}

module.exports.main = async (event, context) => {
	try {
		console.log(event);
		const { corporationId, userId, body } = getInputs(event);
		// TODO: validate OTP
		const savedMemberFile = await CorporationUser.update(body, {
			where: {
				id: userId,
                corporationId
			},
		});
		return {
			statusCode: 200,
			body: JSON.stringify(savedMemberFile),
		};
	} catch (error) {
		return {
			statusCode: 500,
			body: JSON.stringify({
				error: error.message,
			}),
		};
	}
};
