const { Corporation } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		corporationId: event.pathParams.corporationId,
		body: event.body,
	};
}

module.exports.main = async (event, context) => {
	try {
		console.log(event);
		const { corporationId, body } = getInputs(event);
		// TODO: validate OTP
		const savedCorporationProfile = await Corporation.update(body, {
			where: {
				id: corporationId,
			},
		});
		return {
			statusCode: 200,
			body: JSON.stringify(savedCorporationProfile),
		};
	} catch (error) {
		return {
			statusCode: 500,
			body: JSON.stringify({
				error: error.message,
			}),
		};
	}
};
