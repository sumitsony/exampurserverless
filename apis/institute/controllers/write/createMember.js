const { CorporationUser } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
        instituteId: event.pathParams.instituteId,
        body: event.body
	};
}

module.exports.main = async (event, context) => {
    try{
        console.log(event);
        const { body } = getInputs(event);
        // TODO: validate OTP
        const savedCorporationUser = await CorporationUser.create(body);
        return {
            statusCode: 200,
            body: JSON.stringify(savedCorporationUser)
        };    
    }
    catch(error) {
        return {
            statusCode: 500,
            body: JSON.stringify({
                error: error.message
            }),
        }
    }

};
