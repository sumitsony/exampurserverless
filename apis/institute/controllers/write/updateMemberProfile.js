const { InstituteUser } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		instituteId: event.pathParams.instituteId,
        userId: event.pathParams.userId,
		body: event.body,
	};
}

module.exports.main = async (event, context) => {
	try {
		console.log(event);
		const { instituteId, userId, body } = getInputs(event);
		// TODO: validate OTP
		const savedMemberProfile = await InstituteUser.update(body, {
			where: {
				id: userId,
                instituteId
			},
		});
		return {
			statusCode: 200,
			body: JSON.stringify(savedMemberProfile),
		};
	} catch (error) {
		return {
			statusCode: 500,
			body: JSON.stringify({
				error: error.message,
			}),
		};
	}
};
