const {
	Institute
} = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		instituteId: event.queryStringParameters.instituteId || 0,
    };
}
module.exports.main = async (event, context) => {
	console.log(event);
	const { 
        instituteId
    } = getInputs(event);
	const institutionDetails = await Institute.findOne({
		where: {
			id: instituteId
		}
	});
	return {
		statusCode: 200,
		body: JSON.stringify(institutionDetails),
	};
};
