const {
	InstituteUser
} = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		instituteUserId: event.queryStringParameters.instituteUserId || 0,
    };
}
module.exports.main = async (event, context) => {
	console.log(event);
	const { 
        instituteUserId
    } = getInputs(event);
	const instituteUserProfile = await InstituteUser.findOne({
		where: {
			id: instituteUserId
		}
	});
	return {
		statusCode: 200,
		body: JSON.stringify(instituteUserProfile),
	};
};
