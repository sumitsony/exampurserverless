const { InstituteUser } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		instituteId: event.queryStringParams.instituteId || null,
		limit: event.queryStringParams.limit || 10,
		offset: event.queryStringParams.offset || 0
	};
}

module.exports.main = async (event, context) => {
	console.log(event);
	const { instituteId, limit, offset } = getInputs(event);
	let instituteUsers = [];
	if (instituteId) {
		instituteUsers = await InstituteUser.findAll({
			where: {
				instituteId
			},
			limit: Math.min(limit, 10),
            offset
		});
	}
	return {
		statusCode: 200,
		body: JSON.stringify(instituteUsers),
	};
};
