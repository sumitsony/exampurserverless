const {
	StudentUser,
	StudentEducationHistory,
	StudentProfessionalHistory,
	StudentSocialProfile,
} = require("@campushyre/lib/db/models");

function getStudentId(event) {
	return {
		studentId: event.queryStringParameters.studentId || 0,
	};
}
module.exports.main = async (event, context) => {
	console.log(event);
	const { studentId } = getStudentId(event);
	const studentProfile = await StudentUser.findAll({
		where: {
			id: studentId,
		},
		include: [
			{
				model: StudentEducationHistory,
			},
			{
				model: StudentProfessionalHistory,
			},
			{
				model: StudentSocialProfile,
			},
		],
	});
	return {
		statusCode: 200,
		body: JSON.stringify(studentProfile),
	};
};
