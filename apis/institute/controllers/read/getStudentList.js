const { StudentUser } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		instituteId: event.queryStringParams.instituteId || "",
	};
}

module.exports.main = async (event, context) => {
	console.log(event);
	const { instituteId, limit, offset } = getInputs(event);
	let studentUsersByInstituteId = [];
	if (instituteId) {
		studentUsersByInstituteId = await StudentUser.findAll({
			where: {
				instituteId
			},
			limit: Math.min(limit, 10),
            offset
		});
	}
	return {
		statusCode: 200,
		body: JSON.stringify(studentUsersByInstituteId),
	};
};
