const { Job, JobApplicableInstitute } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		instituteId: event.queryStringParams.instituteId || "",
	};
}

module.exports.main = async (event, context) => {
	console.log(event);
	const { instituteId, limit, offset } = getInputs(event);
	let jobsByInstituteId = [];
	if (instituteId) {
		jobsByInstituteId = await Job.findAll({
			where: {
				instituteId
			},
            include: {
                model: JobApplicableInstitute,
                required: true,
                where: {
                    instituteId
                }
            },
			limit: Math.min(limit, 10),
            offset
		});
	}
	return {
		statusCode: 200,
		body: JSON.stringify(jobsByInstituteId),
	};
};
