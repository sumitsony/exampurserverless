const { Application } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		jobId: event.queryStringParams.jobId || "",
	};
}

module.exports.main = async (event, context) => {
	console.log(event);
	const { jobId, limit, offset } = getInputs(event);
	let applicationsByJobId = [];
	if (jobId) {
		applicationsByJobId = await Application.findAll({
			where: {
				jobId
			},
			limit: Math.min(limit, 10),
            offset
		});
	}
	return {
		statusCode: 200,
		body: JSON.stringify(applicationsByJobId),
	};
};
