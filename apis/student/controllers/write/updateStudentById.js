// title
// firstName
// lastName
// mobile:
// currentCity

// lookingFor: [] internship/job
// suitableWorkEnvironment: []
// interestedFields: []

const { StudentUser } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		studentId: event.pathParams.studentId,
		body: event.body,
	};
}

module.exports.main = async (event, context) => {
	try {
		console.log(event);
		const { studentId, body } = getInputs(event);
		// TODO: validate OTP
		const savedStudentUser = await StudentUser.update(body, {
			where: {
				id: studentId,
			},
		});
		return {
			statusCode: 200,
			body: JSON.stringify(savedStudentUser),
		};
	} catch (error) {
		return {
			statusCode: 500,
			body: JSON.stringify({
				error: error.message,
			}),
		};
	}
};
