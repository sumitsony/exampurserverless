// title
// firstName
// lastName
// mobile:
// currentCity

// lookingFor: [] internship/job
// suitableWorkEnvironment: []
// interestedFields: [] 

const { StudentUser } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
        body: event.body
	};
}

module.exports.main = async (event, context) => {
    try{
        console.log(event);
        const { body } = getInputs(event);
        // TODO: validate OTP
        const savedStudentUser = await StudentUser.create(body);
        return {
            statusCode: 200,
            body: JSON.stringify(savedStudentUser)
        };    
    }
    catch(error) {
        return {
            statusCode: 500,
            body: JSON.stringify({
                error: error.message
            }),
        }
    }

};
