// send canApply
const {
	Application,
    ApplicationAnswer
} = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		applicationId: event.queryStringParameters.applicationId || 0,
        studentId: event.queryStringParameters.studentId || 0
	};
}
module.exports.main = async (event, context) => {
	console.log(event);
	const { 
        applicationId,
        studentId
    } = getInputs(event);
	const applicationDetails = await Application.findOne({
		where: {
			id: applicationId,
            studentId: studentId
		},
        include: {
            model: ApplicationAnswer
        }
	});
	return {
		statusCode: 200,
		body: JSON.stringify(applicationDetails),
	};
};
