const { Location, Sequelize } = require("@campushyre/lib/db/models");
const { Op } = Sequelize;

function getInputs(event) {
	return {
		suggestQuery: event.queryStringParams.suggestQuery || "",
	};
}

module.exports.main = async (event, context) => {
	console.log(event);
	const { suggestQuery } = getInputs(event);
	let locationSuggestions = [];
	if (suggestQuery) {
		locationSuggestions = await Location.findAll({
			attributes: ["id", "name"],
			where: {
				status: "ACTIVE",
				name: {
					[Op.iLike]: `${suggestQuery}%`,
				},
			},
			limit: 10,
		});
	}
	return {
		statusCode: 200,
		body: JSON.stringify(locationSuggestions),
	};
};
