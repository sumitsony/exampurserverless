// send canApply
const {
	Job,
} = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		jobId: event.queryStringParameters.jobId || 0,
        studentId: event.queryStringParameters.studentId
	};
}

function getApplicability(jobId, studentId) {
    return true; // implement is applicable
}
module.exports.main = async (event, context) => {
	console.log(event);
	const { 
        jobId,
        studentId
    } = getInputs(event);
    const applicability = await getApplicability(jobId, studentId);
	const jobDetails = await Job.findOne({
		where: {
			id: jobId,
		}
	});
    jobDetails.studentApplicable = applicability;
	return {
		statusCode: 200,
		body: JSON.stringify(jobDetails),
	};
};
