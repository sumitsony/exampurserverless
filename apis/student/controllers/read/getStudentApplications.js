const { Application } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		studentId: event.queryStringParams.studentId || "",
	};
}

module.exports.main = async (event, context) => {
	console.log(event);
	const { studentId, limit, offset } = getInputs(event);
	let studentApplications = [];
	if (studentId) {
		studentApplications = await Application.findAll({
			where: {
				studentId
			},
			limit: Math.min(limit, 10),
            offset
		});
	}
	return {
		statusCode: 200,
		body: JSON.stringify(studentApplications),
	};
};
