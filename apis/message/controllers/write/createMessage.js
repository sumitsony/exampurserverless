// create on shortlist
const { Message, Sequelize } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		messageThreadId: event.pathParameters.messageThreadId || "",
        body: event.body
	};
}

module.exports.main = async (event, context) => {
    try{
        console.log(event);
        const { messageThreadId, body } = getInputs(event);
        if (messageThreadId) {
            const savedMessage = await Message.create({
                messageThreadId,
                ...body
            });
            return {
                statusCode: 200,
                body: JSON.stringify(savedMessage),
            };    
        }
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: 'messageThreadId is required for creating message'
            }),
        };
    }
    catch(error) {
        return {
            statusCode: 500,
            body: JSON.stringify({
                error: error.message
            }),
        }
    }

};
