const { Message } = require("@campushyre/lib/db/models");

function getInputs(event) {
	return {
		messageThreadId: event.queryStringParams.messageThreadId || "",
	};
}

module.exports.main = async (event, context) => {
	console.log(event);
	const { messageThreadId, limit, offset } = getInputs(event);
	let messagesByThreadId = [];
	if (messageThreadId) {
		messagesByThreadId = await Message.findAll({
			where: {
				messageThreadId
			},
			limit: Math.min(limit, 10),
            offset
		});
	}
	return {
		statusCode: 200,
		body: JSON.stringify(messagesByThreadId),
	};
};
