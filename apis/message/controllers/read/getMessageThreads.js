const { MessageThread, Message, Sequelize } = require("@campushyre/lib/db/models");
const {Op} = Sequelize;

function getInputs(event) {
	return {
		userId: event.queryStringParams.userId || "",
	};
}

module.exports.main = async (event, context) => {
	console.log(event);
	const { userId, limit, offset } = getInputs(event);
	let messageThreadsByUserId = [];
	if (userId) {
		messageThreadsByUserId = await MessageThread.findAll({
			where: {
                [Op.or] :[
                    {senderId: userId},
                    {recieverId: userId}
                ]
			},
            include: {
                model: Message,
                limit: 5
            },
			limit: Math.min(limit, 10),
            offset
		});
	}
	return {
		statusCode: 200,
		body: JSON.stringify(messageThreadsByUserId),
	};
};
