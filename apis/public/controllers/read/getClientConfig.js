const _ = require('lodash');
const {ClientConfig} = require('@campushyre/lib/db/models');
const {generateResponse} = require('@campushyre/lib/utils');
module.exports.main = async function(event, context) {
    const clientConfig = await ClientConfig.findAll();
    const responseBody = _.fromPairs(clientConfig.map(({key, value}) => [key, value]));
    return generateResponse({
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Methods': 'OPTIONS,GET'
        },
        body: JSON.stringify(clientConfig) 
    });
}