const { FeaturedItem, Job, Internship } = require("@campushyre/lib/db/models");
const {getCommonCORSHeaders}= require('@campushyre/lib/utils');
module.exports.main = async function (event, context) {
	const featuredItems = await FeaturedItem.findAll({
		where: {
			type: ['JOB', 'INTERNSHIP']
		}
	});
	const [jobs, internships] = _.partition(featuredItems, item => item.type === 'JOB');
	const jobIds = jobs.map(job => job.id);
	const internshipIds = internships.map(internship => internship.id);
	const [
		featuredJobs,
		featuredInternships
	] = await Promise.all([
		Job.findAll({
			where: {
				id: jobIds
			}
		}),
		Internship.findAll({
			where: {
				id: internshipIds
			}
		})
	]);
	const responseBody = {
		jobs: featuredJobs,
		internships: featuredInternships
	};
	return {
		statusCode: 200,
		headers: {
            ...getCommonCORSHeaders(),
            'Access-Control-Allow-Methods': 'OPTIONS,GET'        
        },
		body: JSON.stringify(responseBody)
	};
};
