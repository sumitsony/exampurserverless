const _ = require('lodash');
const {Review} = require('@campushyre/lib/db/models')
const {getCommonCORSHeaders} = require('@campushyre/lib/utils')
module.exports.main = async function(event, context) {
    const reviews = await Review.findAll();
    const responseBody = [
        {
            reviewerDisplayPic,
            reviewerDisplayName,
            reviewerInstituteName,
            reviewerCorporationName,
            review
        }
    ]
    return {
        statusCode: 200,
        headers: {
            ...getCommonCORSHeaders(),
            'Access-Control-Allow-Methods': 'OPTIONS,GET'        
        },
        body: JSON.stringify(responseBody)
    };
}